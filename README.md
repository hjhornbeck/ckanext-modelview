# Projection View (ckanext-modelview)

![A demo of Projection View in action](images/demo.png)

This is a [CKAN](https://ckan.org/) extension that not only allows you to view your 
 datasets, it can overlay multiple models or projections of how that data will behave
 over the data itself. It's based on [Plotly](https://plotly.com/), and in general is 
 more lightweight than reclineview while still offering quite a bit of functionality.

Projection View is very client-side, consisting mostly of Javascript code that does
 all the necessary computation. This can make the performance sluggish on older computers,
 but also means there's minimal server load and any changes to the underlying data are
 reflected after a page refresh in the worst case. Be warned that by default it slurps
 in the entire dataset, so if there are a lot of datapoints don't be surprised if the
 page becomes unresponsive.


## Requirements

This extension depends heavily on DataStore at minimum, and DataPusher or a work-alike 
 are recommended. It comes with a minified version of Plotly, to ensure consistent 
 behaviour.

Compatibility with core CKAN versions:

| CKAN version    | Compatible?   |
| --------------- | ------------- |
| 2.6 and earlier | not tested    |
| 2.7             | not tested    |
| 2.8             | not tested    |
| 2.9             | not tested    |
| 2.9.9           | works         |
| 2.10            | not tested    |


## Installation

To install ckanext-modelview:

1. Activate your CKAN virtual environment, for example:

     . /usr/lib/ckan/default/bin/activate

2. Clone the source and install it on the virtualenv

    git clone https://github.com/hjhornbeck/ckanext-modelview.git
    cd ckanext-modelview
    pip install -e .
	pip install -r requirements.txt

3. Add `modelview` to the `ckan.plugins` setting in your CKAN
   config file (by default the config file is located at
   `/etc/ckan/default/ckan.ini`).

4. Optionally, add `modelview` to `ckan.views.default_views` as well.

5. Restart CKAN. For example if you've deployed CKAN with Apache on Ubuntu:

```
sudo service apache2 reload
```

6. To alert CKAN to the new view, run
```
ckan views clear
```

7. Check the logs for errors about permissions. We observed this after
   first launch on our install of CKAN, and had to manually change the
   permissions of a large number of subdirectories under
   `chmod -R ckan:ckan /var/lib/ckan/webassets/.webassets-cache`. Depending
   on how you've installed CKAN, this may not be necessary.


## Config settings

There are no configuration settings for this extension. 

However, you may find that it doesn't correctly detect which axis a column belongs 
  to. If a column is supposed to represent categorical data, for instance, add 
  "categorical" somewhere in that column's description within the data dictionary. 
  "x-axis" and "y-axis" in the description have predictable effects.


## Usage

The top of the view has a series of drop-down boxes, to select the columns for the X and Y 
  axes. If categorical data is present, it will have a drop-down box as well.
  Filtering the categories displayed can be done by hovering the mouse over the "Categories"
  title, to reveal an input which lists all categories. Clicking selects exactly one, 
  control-clicking adds or removes from the existing selection, and shift-clicking selects a
  range of values. Move the mouse away from the input and wait to get rid of it.

The graph itself is a standard Plotly graph, with no interactions disabled.

Below and to the left is a list of any pre-existing prejections that are available for the 
  chosen X, Y, and categorical columns. This behaves the same as the category filter,
  multiple projections can be displayed simultaneously.

The lower right of the view is a work in progress, and is almost certain to change in future.

## Model setup

Projection View has a standardized format for any model it displays. Suppose you've set up
  the "My Dataset" dataset, and uploaded the main resource you'd like to overlay projections
  over as (say) "Original Data". A projection for that data will be another resource with the name 
  'Original Data - My Model Name (cached model, x: "X_COLUMN", y: "Y_COLUMN", c:"CATEGORICAL")',
  where "My Model Name" is the name of the model, X_COLUMN is the dataset column in "Original 
  Data" containing the independent variable, Y_COLUMN is the column containing the dependent 
  variable, and CATEGORICAL is a column for partitioning the data into independent groups (for
  instance: countries, assigned sexes, sample sites, ...). If no grouping is done, CATEGORICAL
  is blank but the format remains the same (ie. the last portion would read ' c:"")').

This resource should contain two or three columns, depending on whether or not there's a
  categorical column present. The column names must be X_COLUMN, Y_COLUMN, and CATEGORICAL.
  Each row is thus consists of the model's output (Y_COLUMN) for independent variable
  X_COLUMN and group CATEGORICAL.

Projection View supports confidence/credible intervals. If you have the typical 5/95 interval
  on top of a median projection for the "Canada" category, then insert the lower bound as a
  row with the pseudo-category "Canada_05", the upper bound as another row with 
  pseudo-category "Canada_95", and the median prediction as a third row with "Canada_50".
  You are not constrained to 5/95 intervals, nor must you have only one set of intervals,
  the only restriction is that intervals consist of two numeric digits and the lower and
  upper bounds sum to 100.

If you have not grouped the dataset by category but want to enable confidence/credible 
  intervals, add a categorical column with only one category, the name of that category.
  Say this dataset is estimating the world's population: add a third column named
  (say) "world" and associate the 16th quantile with the pseudo-category "world_16",
  the 84th with "world_84", and the median with "world_50".

## Developer installation

To install ckanext-modelview for development, activate your CKAN virtualenv and
do:

    git clone https://github.com/hjhornbeck/ckanext-modelview.git
    cd ckanext-modelview
    python setup.py develop
    pip install -r dev-requirements.txt


## Tests

Currently, no tests have been added. Once they have been in future,
  run the tests with:

    pytest --ckan-ini=test.ini


## Releasing a new version of ckanext-modelview

The best way to create a new version of ckanext-modelview is to clone the repository. It is not
  on PyPI, and in the short term there are no plans to put it there.

## License

[AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html)

