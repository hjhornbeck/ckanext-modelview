# encoding: utf-8

##### IMPORTS

from typing import Any, Callable, Container

from logging import getLogger

from ckan.common import CKANConfig, json, config
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit

import json

##### GLOBALS

log = getLogger(__name__)
res_tags = ('id','name','total_record_count','datastore_active')    # the only resource attributes we care about


##### METHODS


##### CLASSES

class ModelviewPlugin(plugins.SingletonPlugin):
    """
    Provide a data view where a resource's data can be displayed as a Plotly chart,
     and pre-calculated projections can be add/removed as well. Borrows heavily from
     reclineview's code.
    """

    plugins.implements(plugins.IConfigurer, inherit=True)
    plugins.implements(plugins.IResourceView, inherit=True)

    # overrides for IConfigurer
    def update_config(self, config_: CKANConfig):
        """
        Add our paths and assets to CKAN's pool.
        """
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_resource('assets', 'ckanext-modelview')

    # overrides for IResourceView
    def info(self) -> dict[str, Any]:
        """
        Return information about this resource view.
        """
        return {'name': 'modelview',
                'default_title': toolkit._('Projection Explorer'),
                'title': toolkit._('Projection Explorer'),
                'icon': 'chart-bar',
                'filterable': False,        # we handle this ourselves
                'requires_datastore': True,
                }

    def can_view(self, data: dict[str, Any]) -> bool:
        """
        Can we view the given dataset?
        """
        res = data['resource']
        return res.get('datastore_active') or ('_datastore_only_resource' in res.get('url', ''))

    def setup_template_variables(self, ctx: Any, data: dict[str, Any]) -> dict[str,str]:
        """
        Pull in some useful values that we can invoke in the template.
        """

        # Javascript's JSON parser can be slow, and CKAN's JSON converter is buggy,
        #  so do a bit of prefiltering. Caching isn't possible because dictionaries aren't
        #  hashable.
        resource = json.dumps( {x:data['resource'][x] for x in res_tags if x in data['resource']} )
        package = json.dumps( [{x:res[x] for x in res_tags if x in res} # no total_record_count!
                for res in data['package']['resources']] )

        return {'package_json':      package,
                'resource_json':      resource,
                'resource_view_json': json.dumps(data['resource_view']),
                }

    def view_template(self, ctx: Any, data: dict[str, Any]) -> str:
        """
        Point CKAN to the correct template for this view.
        """
        return "modelview.html"


