// create some variables we'll fill in later
var resource_info;
var package_resources;
var summary_id;
var dictionary = new Object(); // {field: {field, type, comment, type_override}}
var x_axis_columns = new Array();
var y_axis_columns = new Array();
var category_columns = new Array();
var projections = new Object(); // {name: [{id, x, y, cat},...]}

var x_limits = new Array();		// useful for creating projections
var category_colours = new Object();	// helps keep a consistent colour palette

var category_selector_state = 0;        // 0 -> invisible, 1 -> visible, 2 -> waiting, 3 -> animating
var cat_header_norm = ['#000','#FFF'];  //  and the current category header's colours

const site = window.location.origin;	// easiest way to grab the access method and site


// https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
function sleep(ms = 1000) { return new Promise(resolve => setTimeout(resolve, ms)); }


// strip away some cruft created by CKAN when converting a dictionary to JSON text
function parse_CKAN_JSON( input ) {

	try {                  // try the easy way first
		return JSON.parse( input );
	}
	catch({ name, message }) {}

	var temp = input;		   // work on a copy, just in case
	var max_tries = 10;        // protect against infinite loops
	while( max_tries > 0 ) {

		try {                  // check if it's valid JSON
			return JSON.parse( temp.substring(1,temp.length-1) );
		}
		catch({ name, message }) {}

		// if not, there must be some quotes which are mistakenly escaped
		temp = temp.replaceAll('\\"', '"');
		max_tries--;
	}
}

// initial setup for the selector
async function init_controls() {

	// pull in information already provided by CKAN
	const container = document.getElementById('modelview-plot-container');
	if( ! container ) {
		console.log( "DEBUG: Could not find modelview's overarching container" );
		return;
		}
	var result;
	result = container.getAttribute('data-module-resource');
	if( ! result ) {
		console.log( "DEBUG: modelview's resource information is missing" );
		return;
		}
	resource_info = parse_CKAN_JSON( result );

	// datastore must be active
	if( ! resource_info.datastore_active ) {
		console.log( "DEBUG: modelview requires DataStore to be active for all datasets" );
		return;
		}
	
	// figure out our summary stats and models
	result = container.getAttribute('data-module-package');
	if( ! result ) {
		console.log( "DEBUG: modelview's package information is missing" );
		return;
		}
	package_resources = parse_CKAN_JSON( result );

	var target_name;			// first pass: save the name of the parent dataset
	for( const res of package_resources ) {

		// filter out summaries and cached models
		var temp = res.name.toString().match( '^(.*) +- +(.*) +\\(cached model, +x *: *"(.*)", *y *: *"(.*)", *c *: *"(.*)"\\)$' );
		if( ! res.datastore_active )		// DataStore must be active!
			continue;
		else if( res.name == resource_info.name + " - Summary Statistics" )
			summary_id = res.id;
		else if( temp && (temp.length == 6) ) {

			// extract the x/y/cat this projection was matched against
			if( Object.hasOwn( projections, temp[2] ) )
				projections[temp[2]].push( new Object({id: res.id, x: temp[3], y: temp[4], c: temp[5]}) );
			else
				projections[temp[2]] = [new Object({id: res.id, x: temp[3], y: temp[4], c: temp[5]})];

			// only the first projection gets to replace the target name
			if ( ! target_name )
				target_name = temp[1];

			}
		}

	// summary_id is blank? check if we're viewing the summary instead of the actual dataset
	if( ! summary_id ) {

		var temp = resource_info.name.toString().match( '^(.*) - Summary Statistics' );
		if( temp && (temp.length == 2) ) {

			if( ! target_name )	// only change this if parsing projections didn't give us a definitive answer
				target_name = temp[1];
			projections['dummy'] = 1; // insert a dummy attribute so we fall through to the viewing-a-projection code

			}
		}

	// check if we're accidentally viewing a projection
	if( (Object.keys(projections).length > 0) && (resource_info.name != target_name) ) {

		// we are, so try to redirect to the parent dataset (we should be in an iframe, after all)
		for( const res of package_resources )
			if( res.name == target_name ) {
				window.location.href = document.URL.replace( resource_info.id, res.id );
				return;
				}

		console.log( 'DEBUG: could not find the parent dataset' );
		return;
		}

	// grab the data dictionary (yes, DataStore is picky and requires a POST request for this one)
	result = await fetch( site + '/api/3/action/datastore_search', 
			                        {method:'POST', headers:{"Content-Type": "application/json"}, 
						body:'{"id": "' + resource_info.id + '","limit":0}'} );
	if( ! result.ok ) {
		console.log( 'DEBUG: could not retrieve the data dictionary for resource ' + resource_info.id );
		return;
		}

	result = await result.json();
	for( const field of result.result.fields ) {	// datastore_search returns comments, which we need for later

		if( Object.hasOwn(field,'info') ) {	// these only exist for data columns
			dictionary[field.info.label] = field.info;
			if( Object.hasOwn(field.info,'type_override') && (field.info.type_override != "") )	// respect type overrides!
				dictionary[field.info.label]['type'] = field.info.type_override;
			else
				dictionary[field.info.label]['type'] = field.type;
			}
		}


	// at long last, we can start categorizing fields
	for( const key of Object.keys(dictionary) ) {

		// flagged as X axis in its description? X axis
		if ( Object.hasOwn(dictionary[key],'notes') && 
			dictionary[key].notes.toString().match("x-axis") )
			x_axis_columns.push( key );

		// flagged as Y axis in its description? Y axis
		else if ( Object.hasOwn(dictionary[key],'notes') && 
			dictionary[key].notes.toString().match("y-axis") )
			y_axis_columns.push( key );

		// flagged as categorical in its description? Categorical
		else if ( Object.hasOwn(dictionary[key],'notes') && 
			dictionary[key].notes.toString().match("categorical") )
			category_columns.push( key );

		// string? automatic categorical
		else if( dictionary[key].type == "text" )
			category_columns.push( key );

		// date? automatic X axis
		else if ( dictionary[key].type == "date" )
			x_axis_columns.push( key );
		else if ( dictionary[key].type == "timestamp" )
			x_axis_columns.push( key );

		// everything else goes on the Y axis
		else
			y_axis_columns.push( key );
		}

	// build the chart widgets
	var text;
	result = document.getElementById('modelview-X-container');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the container for the X axis widget' );
		return;
		}
	
	text = "X&nbsp;axis" + '<select id="modelview-X-control" title="X axis" class="modelview-plot-mods-control">';
	for( var i in x_axis_columns ) {
		text += '<option value="' + x_axis_columns[i] + '"';
		if( i == 0 )
			text += ' selected';
		text += '>' + x_axis_columns[i] + '</option>';
		}
	result.innerHTML = text + '</select>';

	result = document.getElementById('modelview-Y-container');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the container for the Y axis widget' );
		return;
		}
	
	text = "Y&nbsp;axis" + '<select id="modelview-Y-control" title="Y axis" class="modelview-plot-mods-control">';
	for( var i in y_axis_columns ) {
		text += '<option value="' + y_axis_columns[i] + '"';
		if( i == 0 )
			text += ' selected';
		text += '>' + y_axis_columns[i] + '</option>';
		}
	result.innerHTML = text + '</select>';

	// don't generate categories unless there actually are categories
	if( category_columns.length > 0 ) {

		result = document.getElementById('modelview-cat-container');
		if( ! result ) {
			console.log( 'DEBUG: Could not find the container for the category widget' );
			return;
			}
	
		text  = '<div id="modelview-cat-header">Categories</div>';
		text += '<select id="modelview-cat-filter-control" title="Choose specific categories" multiple>';
		text += '</select>';            // since we don't know the actual categories at this point, leave it blank
		text += '<select id="modelview-cat-control" title="Categories" class="modelview-plot-mods-control">';
		text += "Categories" + '<option value="">None</option>';
		for( var i in category_columns ) {
			text += '<option value="' + category_columns[i] + '"';
			if( i == 0 )
				text += ' selected';
			text += '>' + category_columns[i] + '</option>';
			}
		result.innerHTML = text + '</select>';
		}

	// set up the callbacks for various changes
        result = document.getElementById("modelview-X-control");
	if( ! result ) {
		console.log( 'DEBUG: Could not find the X-axis control widget, just after we created it' );
		return;
		}
	result.addEventListener("change", x_axis_change );

        result = document.getElementById("modelview-Y-control");
	if( ! result ) {
		console.log( 'DEBUG: Could not find the Y-axis control widget, just after we created it' );
		return;
		}
	result.addEventListener("change", y_axis_change );

        result = document.getElementById("modelview-cat-control");
	if( result ) 	// this could legitimately be missing, if there's no categories
		result.addEventListener("change", category_change );

        result = document.getElementById("modelview-add-proj-button");
	if( ! result ) {
		console.log( 'DEBUG: Could not find the "add projection" widget' );
		return;
		}
	result.addEventListener("click", add_new_event );

        result = document.getElementById("modelview-cat-header");
	if( result ) {		// this could legitimately be missing, so skip the hooks if it is

		// only replace the old style if it's valid
		if( result.style.color != "" )
			cat_header_norm[0] = result.style.color;
		if( result.style.backgroundColor != "" )
			cat_header_norm[1] = result.style.backgroundColor;

		result.addEventListener("mouseover", (event) => {
        		var result = document.getElementById("modelview-cat-header");		// the outer variable can be clobbered
			if( ! result )
				return;
			result.style.color = cat_header_hi[0];                                  // replace it with hover colours
			result.style.backgroundColor = cat_header_hi[1];
			if( category_selector_state == 0 ) {					// only pop up the filter on invisibility
				var temp = document.getElementById('modelview-cat-filter-control');     // pop up the control immediately
				if( temp ) {
					temp.style.display = 'block';
					category_selector_state = 1;
					}
				}
			} );
		result.addEventListener("mouseleave", (event) => {
        		var result = document.getElementById("modelview-cat-header");
			if( ! result )
				return;
			result.style.color = cat_header_norm[0];                        // set the style back
			result.style.backgroundColor = cat_header_norm[1];              //  but add a fade, because it looks neat
			if( category_selector_state == 1 ) {				// flag for fading if solidly visible
				category_selector_state = 2;
				category_filter_fade();					// toss this into the background
				}
			result.animate( [{'color':cat_header_hi[0],'backgroundColor':cat_header_hi[1]},
					 {'color':cat_header_norm[0],'backgroundColor':cat_header_norm[1]}], 800 );
			} );

		} // if ( category header is present )

	result = document.getElementById("modelview-cat-filter-control");
	if( result ) {		// this can legitimately be missing

		result.addEventListener("change", category_filter_change );             // update plots via filters
		result.addEventListener("mouseleave", (event) => {			// flag for fading
			category_selector_state = 2;
			category_filter_fade();
			} );
		result.addEventListener("mouseover", (event) => {
			var result = document.getElementById("modelview-cat-filter-control");
			if( ! result )
				return;
			if( category_selector_state != 3 ) {		     // cancel everything but a fade
				category_selector_state = 1;
				result.style.display = 'block';
				}
			} );
		}
		
	// finally, trigger an update of the chart
	await axis_change( null, true );
	}

// fade out the category filter after a spell, unless asked not to
async function category_filter_fade( event ) {

	var result = document.getElementById("modelview-cat-filter-control");
	if( ! result )
		return;

	await sleep( 2000 );			// give time for the cancellation signal to arrive
	if( category_selector_state == 2 ) {	// no signal? begin the fade
		category_selector_state = 3;	
		await result.animate([{'opacity':1},{'opacity':0}], 400 ).finished;
		result.style.display = 'none';  // make sure we're disappearnig the filter
		category_selector_state = 0;	// finally, flag invisibility
		}
	}

// when the category filters change
async function category_filter_change( event ) {
	
	// find the category filter control
	var selected = new Object();
	var result = document.getElementById("modelview-cat-filter-control");
	if( ! result ) {
		console.log( 'DEBUG: Could not find the category filter control, though we should have been able to' );
		return;
		}
	
	// convert selected options into a dictionary
	for( var option of result.options )
		if( option.selected )
			selected[option.value] = 1;
	
	result = document.getElementById("modelview-plot-storage");
	if( ! result ) {
		console.log( 'DEBUG: Could not find the plot' );
		return;
		}
	
	// modify chart visibility
	for( var series of result.data ) {
	
		// first search for projections
		var temp = series.name.toString().match( '^(.*)( \\(.*\\))$' );
		if( temp )
			temp = temp[1];
		else
			temp = series.name.toString();		// must be just the category name, otherwise

		series['visible'] = Object.hasOwn( selected, temp );
		}
	
	// trigger a redraw
	Plotly.redraw( result );
	
	}


// when various parts of the axis selectors change
async function axis_change( event, first ) {

	// fetch the X, Y, and category values
	var result;
	result = document.getElementById('modelview-X-control');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the x-axis control widget' );
		return;
		}
	const x_name = result.value;

	result = document.getElementById('modelview-Y-control');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the y-axis control widget' );
		return;
		}
	const y_name = result.value;

	var cat_name;
	result = document.getElementById('modelview-cat-control');
	if( ! result )		// there can be legit reasons this control is missing!
		cat_name = "";	// use a blank string rather than null
	else
		cat_name = result.value;

	// rebuild the projection info
	result = document.getElementById('modelview-proj-available');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the container for the projection widget' );
		return;
		}
	
	// only add projections that match the axis values
	text = "Projections" + '<select id="modelview-proj-control" title="Projections" class="modelview-plot-mods-control" multiple>';
	for( var name of Object.keys(projections) )
		for( var proj of projections[name] )
			if( (proj.x == x_name) && (proj.y == y_name) && ((cat_name == "") || (proj.c == cat_name)) )
				text += '<option value="' + name + '">' + name + '</option>';
	result.innerHTML = text + '</select>';

	// we should re-add the event listener
	result = document.getElementById("modelview-proj-control");
	if( ! result ) {
		console.log( 'DEBUG: Could not find the projection control widget, just after we created it' );
		return;
		}
	result.addEventListener("change", projection_change );

	// finally, fill in the Plotly chart, first by fetching the data to display
	var x = new Array(); var y = new Array(); var c = new Array();

	text = ',"fields":["' + x_name + '","' + y_name;
	if( cat_name != "" )
		text += '","' + cat_name + '"]';
	else
		text += '"]';

	category_colours = new Object();
	var category_count = 0;
	var num_rows = 2000;		// handle the case where the total size isn't known
	if( Object.hasOwn(resource_info,'total_record_count') )
		num_rows = resource_info.total_record_count;

	while ( x.length < num_rows ) {

		result = await fetch( site + '/api/3/action/datastore_search', 
			{method:'POST', headers:{"Content-Type": "application/json"}, 
					body:'{"id": "' + resource_info.id + '","limit":' + 
					(num_rows - x.length) + 
					',"offset":' + x.length + text + '}'} );
		if( ! result.ok ) {
			console.log( 'DEBUG: could not retrieve the last ' + 
				(num_rows - x.length) +
				' rows when filling in the chart' );
			return;
			}
		result = await result.json();

		// backfill the total size
		if( !Object.hasOwn(resource_info,'total_record_count') ) {
			resource_info['total_record_count'] = result.result.total;
			num_rows = result.result.total;
			}
	
		for( var record of result.result.records ) {
			x.push( record[x_name] );
			y.push( record[y_name] );
			if( cat_name != "" ) {
				c.push( record[cat_name] );
				if( !Object.hasOwn(category_colours,record[cat_name]) )		// assign a colour
					category_colours[record[cat_name]] = category_count++ % colours.length;
				}
			else
				c.push( "" );	// we iterate over this array later
			}
		}

	// no saved limits yet? Fill some temporary ones in via this data
	if( x_limits.length < 2 ) {
		x_limits[0] = x[0];
		x_limits[1] = x[x.length-1];
		}

	// hand everything over to Plotly
        result = document.getElementById("modelview-plot-storage");
	if( ! result ) {
		console.log( 'DEBUG: Could not find the storage area for the Plotly chart' );
		return;
		}

	// the default should be splitting by category, if there are categories
	var storage = new Object();
	for( var i in c ) {

		if( ! Object.hasOwn( storage, c[i] ) ) {	// "" is a valid object attribute
			if( c[i] == "" )
				storage[c[i]] = {x: [], y: [], type:'scatter', mode:'lines+markers', 
					name:resource_info.name, _cat:c[i]};  // no need to enforce consistent colouring
			else
				storage[c[i]] = {x: [], y: [], type:'scatter', mode:'lines+markers', name:c[i], _cat:c[i],
					line:{color:colours[category_colours[c[i]]][0]}};
			}
		storage[c[i]].x.push( x[i] );
		storage[c[i]].y.push( y[i] );

		}

	// build the data for Plotly to show, and update the category filter at the same time
	var data = new Array();
	var text = "";
	for( var d of Object.keys(storage) ) {
		data.push( storage[d] );
		text += '<option value="' + d + '" selected>' + d + '</option>';
		}

	var cat_filter = document.getElementById("modelview-cat-filter-control");
	if( cat_filter )
		cat_filter.innerHTML = text;

	// finally, actually display the plot
	if( first ) {
		result.innerHTML = '';		// wipe out anything that previously existed
		Plotly.newPlot( result, data, 
			{title:resource_info.name, xaxis: {title:x_name}, yaxis: {title:y_name}} );
		result.on('plotly_relayout', axis_shift);
		}
	else
		Plotly.react( result, data, 
			{title:resource_info.name, xaxis: {title:x_name}, yaxis: {title:y_name},
			datarevision: x_name + y_name + cat_name} );

	}

// we need to record shifts of the X axis
async function axis_shift( event ) {

	// we only care about x axis shifts
	if( Object.hasOwn(event,"xaxis.range[0]") )
		x_limits[0] = event["xaxis.range[0]"];

	// updates can happen piecemeal, so don't make assumptions
	if( Object.hasOwn(event,"xaxis.range[1]") )
		x_limits[1] = event["xaxis.range[1]"];

	}

// no need to do axis-specific code yet
async function x_axis_change( event ) { await axis_change( event, false ); }
async function y_axis_change( event ) { await axis_change( event, false ); }
async function category_change( event ) { await axis_change( event, false ); }


// when the projection choice changes
async function projection_change( event ) {

	// enumerate all the projections already in use
        const plot = document.getElementById("modelview-plot-storage");
	if( ! plot ) {
		console.log( 'DEBUG: Could not find the storage area for the Plotly chart' );
		return;
		}

	var displayed_proj = new Array();
	for( var i in plot.data ) {

		// this is super-easy with custom properties
		var temp = new Object();
		if( Object.hasOwn( plot.data[i], '_cat' ) )
			temp['cat'] = plot.data[i]._cat;
		if( Object.hasOwn( plot.data[i], '_proj' ) )
			temp['proj'] = plot.data[i]._proj;
		if( Object.hasOwn( plot.data[i], '_quant' ) )
			temp['quant'] = plot.data[i]._quant;

		displayed_proj.push( temp );

		} // for (all displayed datasets)

	// fetch the X, Y, and category values
	var result;
	result = document.getElementById('modelview-X-control');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the x-axis control widget' );
		return;
		}
	const x_name = result.value;

	result = document.getElementById('modelview-Y-control');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the y-axis control widget' );
		return;
		}
	const y_name = result.value;

	var cat_name;
	result = document.getElementById('modelview-cat-control');
	if( ! result )		// there can be legit reasons this control is missing!
		cat_name = "";
	else
		cat_name = result.value;

	// grab all selected values
	result = document.getElementById('modelview-proj-control');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the projection control widget' );
		return;
		}

	var active_proj = new Object();
	for( var option of result.options )
		 if( option.selected ) {

			// track down the correct projection
			for( var cand of projections[option.value] )
				 if( (cand.x == x_name) && (cand.y == y_name) && ((cat_name == "") || (cand.c == cat_name)) ) {
					active_proj[option.value] = cand;
					break;
					}

			}

	// delete projections that are no longer active
	var to_delete = new Array();
	for( var i in displayed_proj )

		if( Object.hasOwn(displayed_proj[i],'proj') &&
			(! Object.hasOwn( active_proj, displayed_proj[i].proj )) )
				to_delete.push( Number(i) );

	// delete active projections that are already displayed
	for( var i in displayed_proj )

		if( Object.hasOwn(displayed_proj[i],'proj') &&
			Object.hasOwn( active_proj, displayed_proj[i].proj ) )
				delete active_proj[displayed_proj[i].proj];

	// defer on actual DOM deletions until error is impossible

	// for all new projections,
	var traces = new Object();
	var trace_order = new Array();

	for( var proj of Object.keys(active_proj) ) {

		// skip the column names, we already know them (in theory)
		// now suck in the entire dataset
		var limit = 2000;		// temporary value, will fix later
		var data = new Array();
		while ( data.length < limit ) {

			result = await fetch( site + '/api/3/action/datastore_search', 
				{method:'POST', headers:{"Content-Type": "application/json"}, 
					body:'{"id": "' + active_proj[proj].id + '","limit":' + (limit - data.length) + 
					',"offset":' + data.length + '}'} );
			if( ! result.ok ) {
				console.log( 'DEBUG: could not retrieve the last ' + (limit - data.length) +
					' rows when filling in the predictions' );
				return;
				}
			result = await result.json();
	
			limit = result.result.total;	// NOW fix this
			for( var record of result.result.records )
				data.push( record );

			}

		// finally, generate the traces
		for( var row of data ) {

			// skip any line which lacks the appropriate columns (excluding the no-category case)
			if( !(Object.hasOwn( row, active_proj[proj].x ) && Object.hasOwn( row, active_proj[proj].y ) &&
					((cat_name == "") || Object.hasOwn( row, active_proj[proj].c ))) )
				continue;

			var cat;		// problem: blank categories
			if( cat_name == "" ) {

				if( result.result.fields.length > 3 )		// if there's enough fields, search for the correct one
					for( var cand of result.result.fields )
						if( Object.hasOwn(cand,'info') && (cand.id != x_name) && (cand.id != y_name) ) {
							cat = row[cand.id];
							break;
							}
				else
					cat = "";				// if not, no category exists

				}
			else
				cat = row[active_proj[proj].c].toString();

			// if we haven't seen this category before, add it
			if( !Object.hasOwn( traces, cat ) ) {

				var quantile = cat.toString().match( '^(.*)_(\\d\\d)' );
				if( ! quantile ) {
				
					// presumably a mean of some sort
					var line_val;
					if( cat_name != "" )
						line_val = {color:trans(category_colours[cat],.5)};
					else
						line_val = {};

					traces[cat] = {x: new Array(), y: new Array(), type:'scatter', mode:'lines', 
						name: cat + " (" + proj + ")", 
						_cat: cat, _proj: proj, line:line_val };
					trace_order.push( cat );
					}

				else if (quantile[2] == '50') {

					// must be a median
					var line_val;
					if( cat_name != "" )
						line_val = {color:trans(category_colours[quantile[1]],.5)};
					else
						line_val = {};

					traces[cat] = {x: new Array(), y: new Array(), type:'scatter', mode:'lines',
						_cat: cat, _proj: proj, _quant: '50', line:line_val,
						name: quantile[1] + " (" + proj + ", median)" };
					trace_order.push( cat );
					}

				else {

					// a credible/confidence interval
					var higher; var lower;
					if( quantile[2] < 50 ) {
						higher = quantile[1] + "_" + (100 - quantile[2]);
						lower  = quantile[1] + "_" + quantile[2];
						}
					else {
						lower = quantile[1] + "_" + (100 - quantile[2]);
						higher  = quantile[1] + "_" + quantile[2];
						}

					var colour;
					if( cat_name != "" )
						colour = trans(category_colours[quantile[1]],.05);
					else
						colour = null;

					traces[higher] = {x: new Array(), y: new Array(), opacity:.1, showlegend:false, mode:'lines',
						_cat: quantile[1], _proj: proj, _quant: higher.substring(higher.length-2),
						line:{width:0,color:colour}, name: quantile[1] + " (" + proj + ", " + quantile[2] + "/" + (100 - quantile[2]) + " interval)"};
					trace_order.push( higher );

					traces[lower] = {x: new Array(), y: new Array(), opacity:.1, fill: 'tonexty', mode:'lines',
						name: quantile[1] + " (" + proj + ", " + quantile[2] + "/" + (100 - quantile[2]) + " interval)",
						_cat: quantile[1], _proj: proj, _quant: lower.substring(lower.length-2),
						line:{width:0,color:colour}};
					trace_order.push( lower );
					}

				}

			// just add to the appropriate trace and be done with it
			traces[cat].x.push( row[active_proj[proj].x] );
			traces[cat].y.push( row[active_proj[proj].y] );

			} // for( row of data )

		} // for( each projection )

	// NOW tidy up the displayed charts
	Plotly.deleteTraces( plot, to_delete );

	// find the category filter control
	var selected = null;
	var result = document.getElementById("modelview-cat-filter-control");
	if( result ) {			// this can be missing for legit reasons
	
		selected = new Object();

		// convert selected options into a dictionary
		for( var option of result.options )
			if( option.selected )
				selected[option.value] = 1;
	
		}

	// and add those new projections, in the proper order
	var temp = new Array();
	for( var name of trace_order ) {

		// try searching for projections with intervals first
		var cat = name.toString().match( '^(.*)(_\\d\\d)$' );
		if( ! cat )
			cat = name.toString();		// then it's a non-interval projection
		else
			cat = cat[1];
		traces[name]['visible'] = (selected == null) || Object.hasOwn( selected, cat );		// no categories = visibility
		temp.push( traces[name] );
		}

	Plotly.addTraces( plot, temp );

	}


// when the "add new" button is clicked
async function add_new_event( event ) {

	// fetch the X, Y, and category values
	var result;
	result = document.getElementById('modelview-X-control');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the x-axis control widget' );
		return;
		}
	const x_name = result.value;

	result = document.getElementById('modelview-Y-control');
	if( ! result ) {
		console.log( 'DEBUG: Could not find the y-axis control widget' );
		return;
		}
	const y_name = result.value;

	var cat_name;
	result = document.getElementById('modelview-cat-control');
	if( ! result )		// there can be legit reasons this control is missing!
		cat_name = "";
	else
		cat_name = result.value;

	// TODO: actualy implement this
	alert(  'X: ' + x_name + '\n' +
		'Y: ' + y_name + '\n' +
		'cat: ' + cat_name + '\n' +
		'observations: ' + x_limits[0] + ' to ' + x_limits[1] );

	}

// flash a little indication that the category header does something
async function flash_cat_header() {

	await sleep( 3000 );
	while( true ) {

		var result = document.getElementById("modelview-cat-header");
		if( result && (category_selector_state == 0) )              // it exists and categories aren't up
			await result.animate( [
				{'color':cat_header_norm[0],'backgroundColor':cat_header_norm[1]},
				{'color':cat_header_hi[0],'backgroundColor':cat_header_hi[1]},
				{'color':cat_header_norm[0],'backgroundColor':cat_header_norm[1]},
				{'color':cat_header_hi[0],'backgroundColor':cat_header_hi[1]},
				{'color':cat_header_norm[0],'backgroundColor':cat_header_norm[1]}], 500 );
		
		await sleep( 8000 );                                  // don't bother for a while
		}
	}


// finally, launch the initialization routine
init_controls().then( flash_cat_header );
