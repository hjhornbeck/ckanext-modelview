const colours = [               // a colourblind-friendly categorical palette, with a tuned transparency weighting
        ['rgb(0,0,0)', 1],	// source: Masataka Okabe and Kei Ito, https://jfly.uni-koeln.de/color/
        ['rgb(230,159,0)', 1],
        ['rgb(86,180,233)', 1],
        ['rgb(0,158,115)', 1],
        ['rgb(240,228,66)', 1],
        ['rgb(0,114,178)', 1],
        ['rgb(213,94,0)', 1],
        ['rgb(204,121,167)', 1]
                ];

// add transparency to the given colour index, respecting the given weighting
function trans( idx, opacity ) { 

	return 'rgba(' + 
		colours[idx][0].substring(4, colours[idx][0].length-1) + 
                ',' + colours[idx][1]*opacity + 
		')'; 
	}

// used for category filter header's highlights
const cat_header_hi = [ 'rgb(0, 51, 102)', 'rgba(255,204,0,.1)' ];
